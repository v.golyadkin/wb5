<!DOCTYPE html>
<html lang="ru">
<head>
  <meta charset="UTF-8">
  <link rel="stylesheet" href="style.css">
  <link rel="stylesheet" href="style.css">
    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
  

  <title>Задание 5</title>
</head>
<body>
  <nav>
    <ul>
      <li><a href="index.php#form" title = "Форма">Форма</a></li>
      <li>
        <?php 
        if(!empty($_COOKIE[session_name()]) && !empty($_SESSION['login']))
          print('<a href="index.php/?quit=1" title = "Выйти">Выйти</a>');
        else
          print('<a href="login.php" title = "Войти">Войти</a>');
        ?>
      </li>
    </ul>
  </nav>
  <div class="main">
    <section id="form">
    <h2>Авторизация</h2>
<?php


header('Content-Type: text/html; charset=UTF-8');


session_start();

if (!empty($_SESSION['login'])) {

  header('Location: ./');
}


if ($_SERVER['REQUEST_METHOD'] == 'GET') {
  if (!empty($_GET['nologin']))
    print("<div>Пользователя с таким логином не существует</div>");
  if (!empty($_GET['wrongpass']))
    print("<div>Неверный пароль!</div>");

?>
<form action="" method="post">
  <label>
    Логин:<br />
    <input name="login" />
  </label> <br />
  <label>
    Пароль:<br />
    <input name="pass" />
  </label> <br />
  <input type="submit" value="Войти" />
</form>
<?php
}

else {
  $user = 'u40087';
  $pass = '2337577';
  $db = new PDO('mysql:host=localhost;dbname=u40087', $user, $pass, array(PDO::ATTR_PERSISTENT => true));
  $stmt1 = $db->prepare('SELECT form_id, pass_hash FROM forms WHERE login = ?');
  $stmt1->execute([$_POST['login']]);
  $row = $stmt1->fetch(PDO::FETCH_ASSOC);
  if (!$row) {
    header('Location: ?nologin=1');
    exit();
  }
  
  $pass_hash = substr(hash("sha256", $_POST['pass']), 0, 20);

  if ($row['pass_hash'] != $pass_hash) {
    header('Location: ?wrongpass=1');
    exit();
  }

  $_SESSION['login'] = $_POST['login'];

  $_SESSION['uid'] = $row['form_id'];

 
  header('Location: ./');
}
?>

</section>
</div>
</body>
</html>
